﻿jdbcUrl = jdbc:mysql://127.0.0.1/superpet?characterEncoding=utf8&zeroDateTimeBehavior=convertToNull
user = root
password = 123456
#password = root
#开发模式，日志输出
devMode = true
#页面模板更新的模式，开发模式可以热更新
engineDevMode = false

#文件上传存放路径for 服务器，本地测试无需配置
#uploadpath = /home/superpet/uploadfile

#微信小程序开发配置信息
appId=wx***************
appSecret=*************************
