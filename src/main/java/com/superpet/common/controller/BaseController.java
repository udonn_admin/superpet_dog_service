package com.superpet.common.controller;

import com.jfinal.core.Controller;
import com.superpet.common.model.Users;
import com.superpet.common.vo.WxSessionVo;
import com.superpet.weixin.api.login.LoginService;

public class BaseController extends Controller {

    protected WxSessionVo getWxSession(){
        return LoginService.me.getWxTokenInfo(getPara("accessToken"));
    }

    protected Long getLoginUserId() {
        return getWxSession().getUserId();
    }

    protected Users getLoginUser(){
        return Users.dao.findById(getWxSession().getUserId());
    }

    public boolean isLogin() {
        return getWxSession() != null;
    }

    public boolean notLogin() {
        return !isLogin();
    }

}
